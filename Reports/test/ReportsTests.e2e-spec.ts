import { expect } from 'chai';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/report/products/:date (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-08')
      .expect(200)
      .then((response) => {
        expect(response.body).to.deep.equal([
          { productName: 'Black sport shoes',
            quantity: 1,
            totalPrice: 110,
          },
          {
            productName: 'Cotton t-shirt XL',
            quantity: 1,
            totalPrice: 25.75,
          },
          {
            productName: 'Blue jeans',
            quantity: 1,
            totalPrice: 55.99,
          },
        ]);
      });
  });

  it('/report/products/:date (GET, no products)', () => {
    return request(app.getHttpServer())
      .get('/report/products/1970-01-01')
      .expect(200)
      .then((response) => {
        expect(response.body).to.be.empty;
      });
  });

  it('/report/customer/:date (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .expect(200)
      .then((response) => {
        expect(response.body).to.deep.equal({
          customerName: 'John Doe',
          totalPrice: 135.75,
        });
      });
  });

  it('/report/customer/:date (GET, no best buyer)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/1970-01-01')
      .expect(404);
  });  
});
