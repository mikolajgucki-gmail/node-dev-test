import { Injectable, Inject } from '@nestjs/common';
import { Product } from '../Model/Product';
import { Order } from '../Model/Order';
import { Repository } from './Repository';

export interface IBestSellingProduct {
  productName: string;
  quantity: number;
  totalPrice: number;
}

export interface IBestBuyer {
  customerName: string;
  totalPrice: number;
}

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  /**
   * Gets the best selling products on a given day.
   * @param date Date.
   * @return Best selling products.
   */
  async getBestSellingProductsByDate(date: string): Promise<IBestSellingProduct[]> {
    const products = await this.repository.getProductsByDate(date);
    const orders = await this.repository.getOrdersByDate(date);
    return products.map(product => {
      const quantity = orders
        .map(order => order.countProductsById(product.getId()))
        .reduce((previous, current) => previous + current);
      return {
        productName: product.getName(),
        quantity,
        totalPrice: quantity * product.getPrice(),
      };
    });
  }

  /**
   * Gets the buyer on a given day with the highest total price.
   * @param date Date.
   * @return Buyer with the highest total price or null if there are no orders
   *     on the day.
   */
  async getBestBuyerByDate(date: string): Promise<IBestBuyer> {
    interface IBuyer {
      customer: number;
      totalPrice: number;
    }

    const products = await this.repository.getProductsByDate(date);
    const orders = await this.repository.getOrdersByDate(date);
    if (!orders.length) {
      return null;
    }

    // calculate total price for each buyer
    const buyers: IBuyer[] = [];
    orders.forEach(order => {
      const customer = order.getCustomer();
      let buyer = buyers.find(buyer => buyer.customer === customer);
      if (!buyer) {
        buyer = { customer, totalPrice: 0 };
        buyers.push(buyer);
      }
      buyer.totalPrice += this.calcOrderTotalPrice(order, products);
    });

    // find the best buyer
    let bestBuyer: IBuyer;
    buyers.forEach(buyer => {
      if (!bestBuyer || buyer.totalPrice > bestBuyer.totalPrice) {
        bestBuyer = buyer;
      }
    });

    const customer = await this.repository.getCustomerById(bestBuyer.customer);
    return { customerName: customer.getFullName(), totalPrice: bestBuyer.totalPrice };
  }

  /**
   * Calculates the total price for an order.
   * @param order Order for which to calculate.
   * @param products Products contained in the order.
   * @return Total price for the order.
   */
  private calcOrderTotalPrice(order: Order, products: Product[]): number {
    return order.getProducts()
      .map(productId => products.find(product => product.getId() === productId).getPrice())
      .reduce((previous, current) => previous + current);
  }
}
