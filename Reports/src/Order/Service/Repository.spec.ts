import { expect } from 'chai';
import 'mocha';
import { Repository } from './Repository';

describe('Repository', () => {
    let uut: Repository;

    beforeAll(() => {
        uut = new Repository();
    });

    it('should return products by date', async () => {
        // when
        const products = await uut.getProductsByDate('2019-08-07');

        // then
        expect(products).to.have.lengthOf(2);
        expect(products[0].getId()).to.equal(1);
        expect(products[1].getId()).to.equal(2);
    });

    it('should return orders by date', async () => {
        // when
        const orders = await uut.getOrdersByDate('2019-08-08');

        // then
        expect(orders).to.have.lengthOf(3);
        expect(orders[0].getNumber()).to.equal('2019/08/1');
        expect(orders[1].getNumber()).to.equal('2019/08/2');
        expect(orders[2].getNumber()).to.equal('2019/08/3');
    });

    it('should return customer of given identifier', async () => {
        // when
        const customer = await uut.getCustomerById(2);

        // then
        expect(customer.getFullName()).to.equal('Jane Doe');
    });
});
