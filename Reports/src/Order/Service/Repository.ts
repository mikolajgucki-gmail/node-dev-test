import { Injectable } from '@nestjs/common';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';

interface OrderDTO {
  number: string;
  customer: number;
  createdAt: string;
  products: number[];
}

interface ProductDTO {
  id: number;
  name: string;
  price: number;
}

interface CustomerDTO {
  id: number;
  firstName: string;
  lastName: string;
}

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<Order[]> {
    return new Promise(resolve => {
      const orders: OrderDTO[] = require('../Resources/Data/orders');
      resolve(orders.map(dto => new Order(dto.number, dto.customer, dto.createdAt, dto.products)));
    });

  }

  fetchProducts(): Promise<Product[]> {
    return new Promise(resolve => {
      const products: ProductDTO[] = require('../Resources/Data/products');
      resolve(products.map(dto => new Product(dto.id, dto.name, dto.price)));
    });
  }

  fetchCustomers(): Promise<Customer[]> {
    return new Promise(resolve => {
      const customers: CustomerDTO[] = require('../Resources/Data/customers');
      resolve(customers.map(dto => new Customer(dto.id, dto.firstName, dto.lastName)));
    });
  }

  async getProductsByDate(date: string): Promise<Product[]> {
    const products = await this.fetchProducts();
    const orders = await this.getOrdersByDate(date);
    return products.filter(product => {
      return orders.some(order => order.containsProduct(product.getId()));
    });
  }

  async getOrdersByDate(date: string): Promise<Order[]> {
    const orders = await this.fetchOrders();
    return orders.filter(order => order.getCreatedAt() === date);
  }

  async getCustomerById(id: number): Promise<Customer> {
    const customers = await this.fetchCustomers();
    return customers.find(customer => customer.getId() === id);
  }
}
