import { expect } from 'chai';
import 'mocha';
import { Test } from '@nestjs/testing';
import { Repository } from './Repository';
import { OrderMapper } from './OrderMapper';

describe('OrderMapper', () => {
    let uut: OrderMapper;

    beforeEach(async () => {
      const module = await Test.createTestingModule({
        providers: [
          Repository,
          OrderMapper,
        ],
      }).compile();
      uut = module.get(OrderMapper);
    });

    it('should return best selling products by date', async () => {
      // when
      const products = await uut.getBestSellingProductsByDate('2019-08-07');

      // then
      expect(products).to.have.lengthOf(2);
      expect(products[0]).to.deep.equal({
        productName: 'Black sport shoes',
        quantity: 2,
        totalPrice: 220,
      });
      expect(products[1]).to.deep.equal({
        productName: 'Cotton t-shirt XL',
        quantity: 1,
        totalPrice: 25.75,
      });
    });

    it('should return best selling products with no orders', async () => {
      // when
      const products = await uut.getBestSellingProductsByDate('1970-01-01');

      // then
      expect(products).to.be.empty;
    });

    it('should return best buyer by date', async () => {
      // when
      const bestBuyer = await uut.getBestBuyerByDate('2019-08-07');

      // then
      expect(bestBuyer).to.deep.equal({ customerName: 'John Doe', totalPrice: 135.75 });
    });

    it('should return null best buyer for no orders', async () => {
      // when
      const bestBuyer = await uut.getBestBuyerByDate('1970-01-01');

      // then
      expect(bestBuyer).to.be.null;
    });
});
