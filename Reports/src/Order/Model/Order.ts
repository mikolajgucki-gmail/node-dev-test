export class Order {
  constructor(private orderNumber: string, private customer: number,
              private createdAt: string, private products: number[]) {            
  }

  getNumber(): string {
    return this.orderNumber;
  }

  getCustomer(): number {
    return this.customer;
  }

  getCreatedAt(): string {
    return this.createdAt;
  }

  getProducts(): number[] {
    return this.products;
  }

  containsProduct(id: number): boolean {
    return this.products.some(productId => productId === id);
  }

  countProductsById(id: number): number {
    return this.products.filter(productId => productId === id).length;
  }
}
