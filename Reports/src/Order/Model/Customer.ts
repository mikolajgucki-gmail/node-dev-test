export class Customer {
  constructor(private id: number, private firstName: string,
              private lastName: string) {
  }

  getId(): number {
      return this.id;
  }

  getFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }
}
