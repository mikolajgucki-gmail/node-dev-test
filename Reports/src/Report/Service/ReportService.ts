import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers, IBestBuyers } from '../Model/IReports';

interface Buyer {
    customer: number;
    totalPrice: number;
}

@Injectable()
export class ReportService {
  @Inject() orderMapper: OrderMapper;

  async getBestSellersByDate(date: string): Promise<IBestSellers[]> {
    return await this.orderMapper.getBestSellingProductsByDate(date);
  }

  async getBestBuyerByDate(date: string): Promise<IBestBuyers> {
    return await this.orderMapper.getBestBuyerByDate(date);
  }
}
