import { Controller, Inject, Get, Param, NotFoundException } from '@nestjs/common';
import { ReportService } from '../Service/ReportService';
import { IBestSellers, IBestBuyers } from '../Model/IReports';

@Controller()
export class ReportController {
  @Inject() reportService: ReportService;

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string): Promise<IBestSellers[]> {
    return await this.reportService.getBestSellersByDate(date);
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string): Promise<IBestBuyers> {
    const buyer = await this.reportService.getBestBuyerByDate(date);
    if (!buyer) {
      throw new NotFoundException('Not Found');
    }
    return buyer;
  }
}
