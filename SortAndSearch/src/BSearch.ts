export class BSearch {
    private static instance: BSearch;
    private operations_: number = 0;

    private constructor() {
    }

    public static get Instance(): BSearch {
        return BSearch.instance || (BSearch.instance = new BSearch());
    }

    /**
     * Finds an index of a value in an array of numbers.
     * @param items Items to search.
     * @param value Value to find index of.
     * @return Index of the value of -1 if it does not exist in the array.
     */
    findIndex(items: number[], value: number): number {
        let low = 0;
        let high = items.length - 1;

        while (low <= high) {
            let middle = low + Math.floor((high - low) / 2);
            if (items[middle] == value) {
                return middle;
            }
            if (items[middle] < value) {
                low = middle + 1;
            }
            else {
                high = middle - 1;
            }
            this.operations_++;
        }
        return -1;
    }    

    /**
     * Gets number of operations performed so far.
     * @return Number of operations performed so far.
     */
    get operations(): number {
        return this.operations_;
    }
}