import { Sort } from './Sort';

interface Range {
    low: number;
    high: number;
}

/**
 * An implementation of the Sort class using the iterative quick sort algorithm.
 */
export class BSort extends Sort {
    /**
     * Sorts in place an array of integers.
     * @param items Items to sort.
     */     
    sort(items: number[]): void {
        const stack: Range[] = [];
        stack.push({ low: 0, high: items.length - 1 });
    
        while (stack.length) {
            const range = stack.pop();
            const position = this.partition(items, range);
    
            if (position - 1 > range.low) { 
                stack.push({ low: range.low, high: position - 1})
            } 
    
            if (position + 1 < range.high) { 
                stack.push({ low: position + 1, high: range.high });
            } 
        } 
    }

    /**
     * Takes the last element as a pivot. Moves all items lower than or equal to
     * the pivot left to the pivot and the remaining items right to the pivot.
     * @param items Items to partition.
     * @param range Range with low and high indexs to partition.
     * @return Index of the pivot.
     */
    private partition(items: number[], range: Range): number {
        const pivot = items[range.high]; 
        let i = (range.low - 1);
      
        for (let j = range.low; j <= range.high - 1; j++) { 
            if (items[j] <= pivot) { 
                i++; 
                BSort.swap(items, i, j);
            } 
        } 
        BSort.swap(items, i + 1, range.high);
        return i + 1; 
    }

    /**
     * Swaps two items within an array.
     * @param items Items to swap.
     * @param i Index of the first item to swap.
     * @param j Index of the second item to swap.
     */
    private static swap(items: number[], i: number, j: number): void {
        const tmp = items[i];
        items[i] = items[j];
        items[j] = tmp;
    }
}