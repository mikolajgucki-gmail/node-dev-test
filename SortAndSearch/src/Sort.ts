/**
 * A superclass for classes implementing sorting algorithms.
 */
export abstract class Sort {
    /**
     * Sorts in place an array of integers.
     * @param items Items to sort.
     */ 
    abstract sort(items: number[]): void;
}