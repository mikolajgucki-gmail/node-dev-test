import { Sort } from './Sort';

/**
 * An implementation of the Sort class using the bubble sort algorithm.
 */
export class ASort extends Sort {
    /**
     * Sorts in place an array of integers.
     * @param items Items to sort.
     */
    sort(items: number[]): void {
        for (let i = 0; i < items.length; i++) {
            let swapped = false;
            for (let j = 0; j < items.length - i - 1; j++) {
                if (items[j] > items[j + 1]) {
                    const tmp = items[j + 1];
                    items[j  + 1] = items[j];
                    items[j] = tmp;
                    swapped = true;
                }
            }
            if (!swapped) {
                return;
            }
        }
    }
}