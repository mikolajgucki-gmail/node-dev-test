import { expect } from 'chai';
import 'mocha';
import { BSearch } from '../src/BSearch';

describe('BSearch', () => {
    it('should find index of the middle item', () => {
        // given
        const items: number[] = [ 1, 2, 3, 4, 5 ];

        // when
        const index = BSearch.Instance.findIndex(items, 3);

        // then
        expect(index).to.equal(2);
    });

    it('should find index of the first item in an odd-length array', () => {
        // given
        const items: number[] = [ -100, -10, 0, 10, 100 ];

        // when
        const index = BSearch.Instance.findIndex(items, -100);

        // then
        expect(index).to.equal(0);
    });

    it('should find index of the first item in an even-length array', () => {
        // given
        const items: number[] = [ -100, -10, 0, 1, 10, 100 ];

        // when
        const index = BSearch.Instance.findIndex(items, -100);

        // then
        expect(index).to.equal(0);
    });

    it('should find index of the last item in an odd-length array', () => {
        // given
        const items: number[] = [ 1, 2, 4, 8, 16, 32, 64, 128, 256 ];

        // when
        const index = BSearch.Instance.findIndex(items, 256);

        // then
        expect(index).to.equal(8);
    });    

    it('should find index of the last item in an even-length array', () => {
        // given
        const items: number[] = [ 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 ];

        // when
        const index = BSearch.Instance.findIndex(items, 512);

        // then
        expect(index).to.equal(9);
    });    
})