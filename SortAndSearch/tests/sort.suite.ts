import { expect } from 'chai';
import 'mocha';
import { Sort } from '../src/Sort';

let currentRandomValue: number;

function random(): number {
    currentRandomValue = (currentRandomValue * 569 + 239) % 65536;
    return currentRandomValue;
}

export default (name: string, uut: Sort) => {
    beforeEach(() => {
        currentRandomValue = 137;
    });

    describe(name, () => {
        it('should sort array with ascending order', () => {
            // given
            const items = [-34, -12, 0, 1, 2, 3, 10, 69, 123, 3495, 10491];

            // when
            uut.sort(items);

            //then
            expect(items).to.deep.equal([-34, -12, 0, 1, 2, 3, 10, 69, 123, 3495, 10491]);
        });

        it('should sort array with descending order', () => {
            // given
            const items = [123, 65, -6];

            // when 
            uut.sort(items);

            // then
            expect(items).to.deep.equal([-6, 65, 123]);
        });

        it('should sort array with minimum at the end', () => {
            // given
            const items = [-12, 0, 1, 2, 3, 10, 69, 123, 3495, 10491, -34];

            // when
            uut.sort(items);

            // then
            expect(items).to.deep.equal([-34, -12, 0, 1, 2, 3, 10, 69, 123, 3495, 10491]);
        });

        it('should sort array with maximum at the beginning', () => {
            // given
            const items = [10491, -34, -12, 0, 1, 2, 3, 10, 69, 123, 3495];

            // when
            uut.sort(items);

            // then
            expect(items).to.deep.equal([-34, -12, 0, 1, 2, 3, 10, 69, 123, 3495, 10491]);
        });

        it('should sort mixed array', () => {
            // given
            const items = [34, -12, 56, 10, -345, 1023, 89, 0, 0, -345, 98123, -1];

            // when
            uut.sort(items);

            // then
            expect(items).to.deep.equal([-345, -345, -12, -1, 0, 0, 10, 34, 56, 89, 1023, 98123]);
        });

        it('should sort big array', () => {
            // given
            const length = 16384;
            const items: number[] = [];
            for (let i = 0; i < length; i++) {
                items.push(random());
            }

            // when
            uut.sort(items);

            // then
            for (let i = 0; i < length - 1; i++) {
                expect(items[i]).to.be.most(items[i + 1]);
            }
        });
    });
}